from django.contrib import admin
from .models import Type, Speciality, Surgoen, Nurse, Admin_Assitant, Surgoen_Team

# Register your models here.
admin.site.register(Type)
admin.site.register(Speciality)
admin.site.register(Surgoen)
admin.site.register(Nurse)
admin.site.register(Admin_Assitant)
admin.site.register(Surgoen_Team)
