from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator #If I change the onLeave, to move it away

# Create your models here.

class Type(models.Model):
    name = models.CharField(max_length=25)

    def __str__(self):
        return self.name


class Speciality(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name




class Surgoen(models.Model):
    profilePicture = models.CharField(max_length=200, blank=True)
    firstName = models.CharField(max_length=25)
    lastName = models.CharField(max_length=25)
    type = models.ForeignKey(Type, related_name='surgoen', on_delete=models.CASCADE)
    onLeave = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(1)]) #for now, 0 is not onLeave and 1 is onLeave
    specialities = models.ForeignKey(Speciality, related_name='surgoen', on_delete=models.CASCADE)
    biography = models.TextField()

    def __str__(self):
        return f'{self.firstName} {self.lastName}, type {self.type}'

class Nurse(models.Model):
    profilePicture = models.CharField(max_length=200, blank=True)
    firstName = models.CharField(max_length=25)
    lastName = models.CharField(max_length=25)
    type = models.ForeignKey(Type, related_name='nurse', on_delete=models.CASCADE)
    onLeave = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(1)]) #for now, 0 is not onLeave and 1 is onLeave
    biography = models.TextField()

    def __str__(self):
        return f'{self.firstName} {self.lastName}, type {self.type}'

class Admin_Assitant(models.Model):
    profilePicture = models.CharField(max_length=200, blank=True)
    firstName = models.CharField(max_length=25)
    lastName = models.CharField(max_length=25)
    type = models.ForeignKey(Type, related_name='admin_assitant', on_delete=models.CASCADE)
    onLeave = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(1)]) #for now, 0 is not onLeave and 1 is onLeave
    biography = models.TextField()

    def __str__(self):
        return f'{self.firstName} {self.lastName}, type {self.type}'


class Surgoen_Team(models.Model):
    surgoen = models.ForeignKey(Surgoen, related_name="surgoen_team", unique=True, on_delete=models.CASCADE)
    admin_assitant = models.ForeignKey(Admin_Assitant, related_name="surgoen_team", on_delete=models.CASCADE, blank=True, null=True)
    nurses = models.ManyToManyField(Nurse, related_name="surgoen_team")
